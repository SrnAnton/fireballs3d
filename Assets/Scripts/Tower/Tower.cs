﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent((typeof(TowerBuilder)))]
public class Tower : MonoBehaviour
{
    private TowerBuilder _builder;
    private List<Block> _blocks;

    public event UnityAction<int> SizeUpdate;

    private void Start()
    {
        _builder = GetComponent<TowerBuilder>();
        _blocks = _builder.Build();

        foreach (var block in _blocks)
        {
            block.BulletHit += OnBulletHit;
        }
        SizeUpdate?.Invoke(_blocks.Count);
    }

    private void OnBulletHit(Block hitedBlock)
    {
        hitedBlock.BulletHit -= OnBulletHit;
        _blocks.Remove(hitedBlock);

        foreach (var block in _blocks)
        {
            block.transform.position = GetNewBlockPosition(block.transform);
        }
        SizeUpdate?.Invoke(_blocks.Count);
    }

    private Vector3 GetNewBlockPosition(Transform current)
    {
        return new Vector3(current.position.x, current.position.y - current.localScale.y , current.position.z);
    }
}
